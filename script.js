var gCoursesDB = {
  description: "This DB includes all courses in system",
  courses: [
    {
      id: 1,
      courseCode: "FE_WEB_ANGULAR_101",
      courseName: "How to easily create a website with Angular",
      price: 750,
      discountPrice: 600,
      duration: "3h 56m",
      level: "Beginner",
      coverImage: "images/courses/course-angular.jpg",
      teacherName: "Morris Mccoy",
      teacherPhoto: "images/teacher/morris_mccoy.jpg",
      isPopular: false,
      isTrending: true
    },
    {
      id: 2,
      courseCode: "BE_WEB_PYTHON_301",
      courseName: "The Python Course: build web application",
      price: 1050,
      discountPrice: 900,
      duration: "4h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-python.jpg",
      teacherName: "Claire Robertson",
      teacherPhoto: "images/teacher/claire_robertson.jpg",
      isPopular: false,
      isTrending: true
    },
    {
      id: 5,
      courseCode: "FE_WEB_GRAPHQL_104",
      courseName: "GraphQL: introduction to graphQL for beginners",
      price: 850,
      discountPrice: 650,
      duration: "2h 15m",
      level: "Intermediate",
      coverImage: "images/courses/course-graphql.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: false
    },
    {
      id: 6,
      courseCode: "FE_WEB_JS_210",
      courseName: "Getting Started with JavaScript",
      price: 550,
      discountPrice: 300,
      duration: "3h 34m",
      level: "Beginner",
      coverImage: "images/courses/course-javascript.jpg",
      teacherName: "Ted Hawkins",
      teacherPhoto: "images/teacher/ted_hawkins.jpg",
      isPopular: true,
      isTrending: true
    },
    {
      id: 8,
      courseCode: "FE_WEB_CSS_111",
      courseName: "CSS: ultimate CSS course from beginner to advanced",
      price: 750,
      discountPrice: 600,
      duration: "3h 56m",
      level: "Beginner",
      coverImage: "images/courses/course-javascript.jpg",
      teacherName: "Juanita Bell",
      teacherPhoto: "images/teacher/juanita_bell.jpg",
      isPopular: true,
      isTrending: true
    },
    {
      id: 14,
      courseCode: "FE_WEB_WORDPRESS_111",
      courseName: "Complete Wordpress themes & plugins",
      price: 1050,
      discountPrice: 900,
      duration: "4h 30m",
      level: "Advanced",
      coverImage: "images/courses/course-wordpress.jpg",
      teacherName: "Clevaio Simon",
      teacherPhoto: "images/teacher/clevaio_simon.jpg",
      isPopular: true,
      isTrending: false
    }
  ]
}
$(document).ready(function () {

  function loadDataToPopular(data) {
    var content = "";
    for (var bI = 0; bI < data.length; bI++) {
      if (data[bI].isPopular == true) {
        content += `<div id=${data[bI].id} class="card mx-2" style="width: 16rem;">
        <img class="card-img-top" src=${data[bI].coverImage} alt="Card image cap">
        <div class="card-body">
          <a class="card-title" href="#">${data[bI].courseName}</a>
          <div class="row">
            <p class="card-text ml-3"><i class="fa-regular fa-clock"></i>${data[bI].duration}</p>
            <p class="ml-3">${data[bI].level}</p>
          </div>
          <div class="row">
            <p class="card-text ml-2">$ ${data[bI].discountPrice}</p>
            <p class="ml-2 del">$ ${data[bI].price}</p>
          </div>
        </div>
        <div class="borderT pt-2">
          <img class="rounded-circle pic mb-2 mx-3" src=${data[bI].teacherPhoto}>
          <a>Claire Robertson</a>
          <i class="fas fa-inbox icon"></i>
        </div>
      </div>
    </div>
  </div>`
      }
    }
    $("#popular").append(content);
  }
  loadDataToPopular(gCoursesDB.courses);


  function loadDataToTreding(data) {
    var content = "";
    for (var bI = 0; bI < data.length; bI++) {
      if (data[bI].isTrending == true) {
        content += `<div id=${data[bI].id} class="card mx-2" style="width: 16rem;">
                      <img class="card-img-top" src=${data[bI].coverImage} alt="Card image cap">
                      <div class="card-body">
                        <a class="card-title" href="#">${data[bI].courseName}</a>
                        <div class="row">
                          <p class="card-text ml-3"><i class="fa-regular fa-clock"></i>${data[bI].duration}</p>
                          <p class="ml-3">${data[bI].level}</p>
                        </div>
                        <div class="row">
                          <p class="card-text ml-2">$ ${data[bI].discountPrice}</p>
                          <p class="ml-2 del">$ ${data[bI].price}</p>
                        </div>
                      </div>
                      <div class="borderT pt-2">
                        <img class="rounded-circle pic mb-2 mx-3" src=${data[bI].teacherPhoto}>
                        <a>Claire Robertson</a>
                        <i class="fas fa-inbox icon"></i>
                      </div>
                    </div>
                  </div>
                </div>`
      }
    }
    $("#trending").append(content);
  }
  loadDataToTreding(gCoursesDB.courses);
});